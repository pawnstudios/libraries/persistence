/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

CREATE TABLE `counting` (
	`id` INT NOT NULL AUTO_INCREMENT,
	`snowflake` BIGINT NOT NULL DEFAULT 0,
	`count` BIGINT NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`),
	UNIQUE INDEX `channelId` (`snowflake`)
)
COLLATE='latin1_swedish_ci'
;
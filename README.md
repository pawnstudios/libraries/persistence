# Persistence

A light-weight Java library that maps database entries to your custom classes using annotations. 

### Features
- Supports SQL databases.
- Uses Flyway for database migrations
- Uses Hikari for database connection retrieval
- Uses a background thread to periodically persist data as specified

### Limitations  

- Currently loads all database rows that have an associated Java class into memory on startup
- Currently lacks a way to unload un-needed data and reload later
- Changes made to the database during program lifetime are ignored, the library assumes there's 
only one instance of it running for a given database and no other programs exist that will modify the database


### Use Cases

- Single instance programs that save state in a private database
- Low profile programs that don't have hundreds of megabytes of stored data 
(Until changes are made to support lazy-loading data and unloading un-needed data)
- Programs that need a low latency persisted data reads
- Writing SQL in Java just completely appalls you with all its tries and its catches 
- A heavy duty ORM like Hibernate feels too complex for your given project
- You also don't understand Hibernate because you didn't take the time to really try it
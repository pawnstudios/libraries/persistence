/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package test.data;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.annotations.Index;

import javax.persistence.*;

@Entity
@Table(name = "counting")
public class CountingData extends PersistedData<CountingData> {
    @Id
    @Column
    int id;
    @Index
    @Column(name = "snowflake")
    long channelId;
    @Column
    long count;

    public CountingData() {
        super();
    }

    public CountingData setChannelId(long channelId) {
        this.channelId = channelId;
        return update("snowflake");
    }

    public CountingData setCount(long count) {
        this.count = count;
        return update("count");
    }
}

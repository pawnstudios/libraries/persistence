/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package test;

import pawnstudios.persistence.api.Entity;
import pawnstudios.persistence.api.Persistence;
import test.data.CountingData;

public class Main {
    static String s;
    public static void main(String... args) throws Exception {
        //New Data
        CountingData newCountingData
          = Entity.create(CountingData.class)
          .setCount((long) 5)
          .setChannelId((long) 10001)
          .initialize();

        //Previously persisted data
        CountingData countingData = Entity.getByIndex("snowflake", (long) 2, CountingData.class);
        CountingData countingData1 = Entity.getById(1, CountingData.class);
        countingData.setCount(501);

        //Gracefully shutdown to ensure all data is persisted before the program exits.
        Persistence.gracefullyShutdown();
        System.out.println("done");

    }
}

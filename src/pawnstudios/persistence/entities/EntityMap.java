/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.entities;

import pawnstudios.persistence.api.PersistedData;

import java.util.Map;

public class EntityMap<T extends PersistedData<T>> {
    public final Class<T> entityClass;
    public final String table;
    public final RecordMapping id;
    public final Map<String, RecordMapping> columns;
    public final Map<String, RecordMapping> indexes;

    public EntityMap(Class<T> entityClass, String table, RecordMapping id, Map<String, RecordMapping> columns, Map<String, RecordMapping> indexes) {
        this.entityClass = entityClass;
        this.table = table;
        this.id = id;
        this.columns = columns;
        this.indexes = indexes;

        setColumnsAccessible();
    }

    public void setColumnsAccessible() {
        id.field.setAccessible(true);
        columns.values().forEach(f -> f.field.setAccessible(true));
        indexes.values().forEach(f -> f.field.setAccessible(true));
    }
}

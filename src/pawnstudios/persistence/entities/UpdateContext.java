/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.entities;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.api.PersistenceException;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class UpdateContext<T extends PersistedData<T>> {
    public final PersistedData<T> persistedData;
    private final Queue<String> updateQueue = new ConcurrentLinkedQueue<>();
    private final Set<String> columnSet = new HashSet<>();

    public UpdateContext(PersistedData<T> persistedData) {
        this.persistedData = persistedData;
    }

    public void queueUpdate(String column) {
        updateQueue.add(column);
    }

    public void prepareCommit() {
        columnSet.clear();
        String column = updateQueue.poll();
        while(column != null) {
            columnSet.add(column);
            column = updateQueue.poll();
        }
    }

    public String generateCreateSQL() {
        String tableName = persistedData.entityMap.table;
        StringBuilder columns = new StringBuilder();
        StringBuilder values = new StringBuilder();
        boolean needsComma = false;
        for (String column : columnSet) {
            if (needsComma) {
                columns.append(", ");
                values.append(", ");
            }
            columns.append("`").append(column).append("`");
            values.append("?");
            needsComma = true;
        }
        return "INSERT INTO `%s` ( %s ) VALUES ( %s )".formatted(tableName, columns, values);
    }

    public String generateUpdateSQL() {
        String tableName = persistedData.entityMap.table;
        String idName = persistedData.entityMap.id.name;
        StringBuilder updatePairs = new StringBuilder();
        boolean needsComma = false;
        for (String column : columnSet) {
            if (needsComma) {
                updatePairs.append(", ");
            }
            updatePairs.append("`%s` = ?".formatted(column));
            needsComma = true;
        }
        return "UPDATE `%s` SET %s WHERE `%s` = ?".formatted(tableName, updatePairs, idName);
    }

    public void fillCreateStatement(PreparedStatement createStmt) throws SQLException {
        fillColumnValues(createStmt);
    }

    public void fillUpdateStatement(PreparedStatement updateStmt) throws SQLException {
        int nextColumnId = fillColumnValues(updateStmt);
        try {
            Object idValue = persistedData.entityMap.id.field.get(persistedData);
            updateStmt.setObject(nextColumnId, idValue);
        } catch (IllegalAccessException e) {
            throw PersistenceException.illegalAccess("?", persistedData.getClass(), e);
        }
    }

    private int fillColumnValues(PreparedStatement ps) throws SQLException {
        int columnId = 1;
        try {
            for (String column : columnSet) {
                RecordMapping recordMapping = persistedData.entityMap.columns.get(column);
                Object value = recordMapping.field.get(persistedData);
                ps.setObject(columnId, value);
                ++columnId;
            }
        } catch (IllegalAccessException e) {
            throw PersistenceException.illegalAccess("?", persistedData.getClass(), e);
        }
        return columnId;
    }
}

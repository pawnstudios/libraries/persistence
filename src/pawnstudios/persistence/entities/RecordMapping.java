/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.entities;

import java.lang.invoke.MethodType;
import java.lang.reflect.Field;

public class RecordMapping {
    public String name;
    public Field field;
    public Class<?> type;

    public RecordMapping(String name, Field field) {
        this.name = name;
        this.field = field;
        type = MethodType.methodType(field.getType()).wrap().returnType();
    }


}

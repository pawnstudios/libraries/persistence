/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.io;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.api.Persistence;
import pawnstudios.persistence.entities.EntityMap;

public class DataCreator {
    public static <T extends PersistedData<T>> T createEntity(Class<T> entityClass) {
        EntityMap<T> entityMap = Persistence.INSTANCE.ENTITY_META_DATA.getEntityMap(entityClass);
        T newInstance = DataLoader.getNewEntityInstance(entityMap);
        Persistence.INSTANCE.RECORD_COLLECTION.addRecord(newInstance);
        return newInstance;
    }
}

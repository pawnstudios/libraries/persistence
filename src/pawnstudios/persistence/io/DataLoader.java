/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.io;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.EntityMap;
import pawnstudios.persistence.entities.RecordMapping;
import pawnstudios.persistence.storage.EntityMetaData;
import pawnstudios.persistence.storage.RecordCollection;

import java.lang.reflect.InvocationTargetException;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

public class DataLoader {
    public static RecordCollection getAllRecords(EntityMetaData entityMetaData) {
        return new RecordCollection(
          entityMetaData.getAllMetaData()
            .values()
            .stream()
            .collect(Collectors.toMap(entityMap -> entityMap.entityClass, DataLoader::loadRecords)));
    }

    public static <T extends PersistedData<T>> List<T> loadRecords(EntityMap<T> entityMap) {
        List<T> records = new LinkedList<>();

        try (Connection con = Database.getDbConnection();
             PreparedStatement ps = con.prepareStatement(String.format("SELECT * FROM `%s`", entityMap.table))) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                T entityInstance = getNewEntityInstance(entityMap);
                populateEntity(entityInstance, entityMap, rs);
                entityInstance.setInitialized();
                records.add(entityInstance);
            }
        } catch (IllegalAccessException | SQLException e) {
            throw new RuntimeException(e);
        }

        return records;
    }

    public static <T extends PersistedData<T>> T getNewEntityInstance(EntityMap<T> entityMap) {
        try {
            T newInstance = entityMap.entityClass.getDeclaredConstructor().newInstance();
            newInstance.entityMap = entityMap;
            return newInstance;
        } catch (InvocationTargetException | InstantiationException | IllegalAccessException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    private static <T extends PersistedData<T>> void populateEntity(T entityInstance, EntityMap<T> entityMap, ResultSet rs) throws IllegalAccessException, SQLException {
        Object id = rs.getObject(entityMap.id.name);
        entityMap.id.field.set(entityInstance, id);

        for (RecordMapping recordMapping : entityMap.columns.values()) {
            Object columnData = rs.getObject(recordMapping.name);
            recordMapping.field.set(entityInstance, columnData);
        }
    }
}

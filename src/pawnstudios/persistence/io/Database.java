/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.io;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pawnstudios.config.ConfigInitializer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static pawnstudios.persistence.config.Persistence.*;
import static pawnstudios.persistence.config.Flyway.*;

public class Database {
    private static HikariDataSource hikariDataSource;
    private static final Logger log = LoggerFactory.getLogger(Database.class);

    static {
        init();
    }

    private static void init() {
        Flyway flyway = Flyway
          .configure()
          .dataSource(url, user, password)
          .locations(migrationLocation)
          .baselineOnMigrate(baselineOnMigration)
          .load();

        try {
            flyway.migrate();
        } catch (FlywayException e) {
            log.error("Database Migration Failed..", e);
            Runtime.getRuntime().exit(1);
        }

        String hikariConfigPath = ConfigInitializer.getConfigPath("hikari");
        HikariConfig hikariConfig = new HikariConfig(hikariConfigPath);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(password);
        hikariDataSource = new HikariDataSource(hikariConfig);
    }

    public static Connection getDbConnection() throws SQLException {
        try {
            return hikariDataSource.getConnection();
        } catch (SQLException e) {
            log.error("Failed to get DB Connection.", e);
            throw e;
        }
    }

    public static DataSource getDataSource() {
        return hikariDataSource;
    }
}

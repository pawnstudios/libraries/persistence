/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.io;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.UpdateContext;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public class WriteBackWorker implements Runnable {
    private final Queue<UpdateContext<?>> recordUpdates = new ConcurrentLinkedQueue<>();
    private final Queue<UpdateContext<?>> recordCreations = new ConcurrentLinkedQueue<>();

    public void run() {
        try {
            processCreations();
            processUpdates();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processCreations() {
        UpdateContext<?> updateContext = recordCreations.poll();
        while (updateContext != null) {
            updateContext.prepareCommit();
            String sql = updateContext.generateCreateSQL();
            try (Connection con = Database.getDbConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                updateContext.fillCreateStatement(ps);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
//                throw new PersistenceException("Failed to persist creation %s".formatted(updateContext.persistedData.getClass().getSimpleName()), e);
            }
            updateContext.persistedData.setInitialized();
            updateContext = recordCreations.poll();
        }
    }

    private void processUpdates() {
       UpdateContext<?> updateContext = recordUpdates.poll();
        while (updateContext != null) {
            updateContext.prepareCommit();
            String sql = updateContext.generateUpdateSQL();
            try (Connection con = Database.getDbConnection(); PreparedStatement ps = con.prepareStatement(sql)) {
                updateContext.fillUpdateStatement(ps);
                ps.executeUpdate();
            } catch (SQLException e) {
                e.printStackTrace();
//                throw new PersistenceException("Failed to persist update %s".formatted(updateContext.persistedData.getClass().getSimpleName()), e);
            }
            updateContext = recordUpdates.poll();
        }
    }

    public void queueRecordUpdate(PersistedData<?> record) {
        recordUpdates.add(record.updateContext);
    }

    public void queueRecordCreation(PersistedData<?> persistedData) {
        recordCreations.add(persistedData.updateContext);
    }
}

/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.storage;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.EntityMap;

import java.util.Map;

public class EntityMetaData {
    private final Map<Class<? extends PersistedData<?>>, EntityMap<? extends PersistedData<?>>> classToEntityMap;

    public EntityMetaData(Map<Class<? extends PersistedData<?>>, EntityMap<? extends PersistedData<?>>> classToEntityMap) {
        this.classToEntityMap = classToEntityMap;
    }

    public <T extends PersistedData<T>> EntityMap<T> getEntityMap(Class<T> entityClass) {
        return (EntityMap<T>) classToEntityMap.get(entityClass);
    }

    public Map<Class<? extends PersistedData<?>>, EntityMap<? extends PersistedData<?>>> getAllMetaData() {
        return classToEntityMap;
    }
}

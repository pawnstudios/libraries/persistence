/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.storage;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.EntityMap;
import pawnstudios.persistence.entities.RecordMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntityIndexer {
    public static IndexedRecords indexAllEntities(EntityMetaData entityMetaData, RecordCollection recordCollection) {
        Map<Class<? extends PersistedData<?>>, Map<String, IndexedCollection<? extends PersistedData<?>>>> allEntries = new HashMap<>();

        for (EntityMap<? extends PersistedData<?>> entityMap : entityMetaData.getAllMetaData().values()) {
            Map<String, IndexedCollection<? extends PersistedData<?>>> indexedMap = new HashMap<>();
            List<? extends PersistedData<?>> records = recordCollection.getRecords(entityMap.entityClass);
            for (RecordMapping recordMapping : entityMap.indexes.values()) {
                addIndexedCollection(indexedMap, recordMapping, records);
            }
            RecordMapping recordMappingId = entityMap.id;
            addIndexedCollection(indexedMap, recordMappingId, records);
            allEntries.put(entityMap.entityClass, indexedMap);
        }

        return new IndexedRecords(allEntries);
    }

    private static void addIndexedCollection(Map<String, IndexedCollection<? extends PersistedData<?>>> indexedMap, RecordMapping recordMapping, List<? extends PersistedData<?>> records) {
        String indexName = recordMapping.name;
        indexedMap.put(indexName, getIndexedCollection(recordMapping, records));
    }

    public static <T extends PersistedData<T>> IndexedCollection<T> getIndexedCollection(RecordMapping index, List<? extends PersistedData<?>> entities) {
        final Map<Object, T> indexToEntity = new HashMap<>();
        try {
            for (Object entity : entities) {
                Object indexValue = index.field.get(entity);
                indexToEntity.put(indexValue, (T) entity);
            }
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        return new IndexedCollection<>(index, indexToEntity);
    }
}

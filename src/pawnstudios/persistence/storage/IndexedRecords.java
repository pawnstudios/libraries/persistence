/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.storage;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.api.Persistence;
import pawnstudios.persistence.api.PersistenceException;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class IndexedRecords {
    private final Map<Class<? extends PersistedData<?>>, Map<String, IndexedCollection<? extends PersistedData<?>>>> allIndexes;

    public IndexedRecords(Map<Class<? extends PersistedData<?>>, Map<String, IndexedCollection<? extends PersistedData<?>>>> allIndexes) {
        this.allIndexes = allIndexes;
    }

    @Deprecated
    public <T extends PersistedData<T>> T getEntity(String index, Object value, Class<T> entityClass) {
        Map<String, IndexedCollection<? extends PersistedData<?>>> indexedCollections = allIndexes.get(entityClass);
        IndexedCollection<? extends PersistedData<?>> indexedCollection = indexedCollections.get(index);
        String expectedType = indexedCollection.index.type.getTypeName();
        String actualType = value.getClass().getTypeName();
        if (!expectedType.equals(actualType)) {
            throw new PersistenceException("Unexpected value should be " + expectedType);
        }
        PersistedData<? extends PersistedData<?>> entity = indexedCollection.indexToEntity.get(value);
        return entity == null ? null : (T) entity;
    }

    public <T extends PersistedData<T>> T getEntitySlow(String index, Object value, Class<T> entityClass) {
        List<T> records = new LinkedList<>(Persistence.INSTANCE.RECORD_COLLECTION.getRecords(entityClass));

        return records
          .stream()
          .filter(record -> {
              try {
               return record.entityMap.columns.get(index).field.get(record).equals(value);
              } catch (IllegalAccessException e) {
                  throw PersistenceException.illegalAccess(index, entityClass, e);
              }
          })
          .findFirst()
          .orElse(null);
    }
}

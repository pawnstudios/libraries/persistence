/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.storage;

import pawnstudios.persistence.api.PersistedData;

import java.util.List;
import java.util.Map;

public class RecordCollection {
    private final Map<Class<? extends PersistedData<?>>, List<? extends PersistedData<?>>> classToData;

    public RecordCollection(Map<Class<? extends PersistedData<?>>, List<? extends PersistedData<?>>> classToData) {
        this.classToData = classToData;
    }

    public Map<Class<? extends PersistedData<?>>, List<? extends PersistedData<?>>> getAllData() {
        return classToData;
    }

    public <T extends PersistedData<T>> List<T> getRecords(Class<T> entityClass) {
        return (List<T>) classToData.get(entityClass);
    }

    public <T extends PersistedData<T>> void addRecord(T record) {

    }
}

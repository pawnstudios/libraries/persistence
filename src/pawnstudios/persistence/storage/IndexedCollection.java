/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.storage;

import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.RecordMapping;

import java.util.Map;

public class IndexedCollection<T extends PersistedData<T>> {
    public final RecordMapping index;
    public final Map<Object, T> indexToEntity;

    public IndexedCollection(RecordMapping index, Map<Object, T> indexToEntity) {
        this.index = index;
        this.indexToEntity = indexToEntity;
    }
}

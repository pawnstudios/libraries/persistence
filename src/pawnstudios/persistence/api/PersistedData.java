/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.api;

import pawnstudios.persistence.entities.EntityMap;
import pawnstudios.persistence.entities.UpdateContext;

public abstract class PersistedData<T extends PersistedData<T>> {
    public EntityMap<T> entityMap;
    public UpdateContext<T> updateContext;
    protected boolean isInitialized;

    protected PersistedData() {
        updateContext = new UpdateContext<>(this);
        isInitialized = false;
    }

    protected T update(String columnName) {
        updateContext.queueUpdate(columnName);
        if (isInitialized) {
            Persistence.INSTANCE.WRITE_BACK_WORKER.queueRecordUpdate(this);
        }
        return (T) this;
    }

    public T initialize() {
        Persistence.INSTANCE.WRITE_BACK_WORKER.queueRecordCreation(this);
        return (T) this;
    }

    public void setInitialized() {
        isInitialized = true;
    }
}

/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.api;

public class PersistenceException extends RuntimeException {
    public PersistenceException(String message) {
        super(message);
    }

    public PersistenceException(String message, Exception cause) {
        super(message, cause);
    }

    public static PersistenceException illegalAccess(String field, Class<?> entityClass, IllegalAccessException e) {
        return new PersistenceException("Failed to access field %s in class %s".formatted(field, entityClass.getSimpleName()), e);
    }
}

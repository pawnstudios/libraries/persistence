/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.api;

import pawnstudios.persistence.annotations.AnnotationsProcessor;
import pawnstudios.persistence.io.DataLoader;
import pawnstudios.persistence.io.WriteBackWorker;
import pawnstudios.persistence.storage.EntityIndexer;
import pawnstudios.persistence.storage.IndexedRecords;
import pawnstudios.persistence.storage.RecordCollection;
import pawnstudios.persistence.storage.EntityMetaData;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Persistence {
    public static final Persistence INSTANCE;

    static {
        INSTANCE = new Persistence();
    }

    public final EntityMetaData ENTITY_META_DATA;
    public final RecordCollection RECORD_COLLECTION;
    public final IndexedRecords INDEXED_RECORDS;
    public final WriteBackWorker WRITE_BACK_WORKER;
    public final ScheduledExecutorService WRITE_BACK_SCHEDULER;

    private Persistence() {
        ENTITY_META_DATA = AnnotationsProcessor.processAnnotations();
        RECORD_COLLECTION = DataLoader.getAllRecords(ENTITY_META_DATA);
        INDEXED_RECORDS = EntityIndexer.indexAllEntities(ENTITY_META_DATA, RECORD_COLLECTION);
        WRITE_BACK_WORKER = new WriteBackWorker();
        WRITE_BACK_SCHEDULER = new ScheduledThreadPoolExecutor(1);
        WRITE_BACK_SCHEDULER.scheduleWithFixedDelay(WRITE_BACK_WORKER, pawnstudios.persistence.config.Persistence.saveIntervalMillis, pawnstudios.persistence.config.Persistence.saveIntervalMillis, TimeUnit.MILLISECONDS);
    }

    public static void touch() {}

    public static void gracefullyShutdown() throws PersistenceException {
        INSTANCE.WRITE_BACK_SCHEDULER.shutdown();

        boolean terminated;
        try {
            terminated = INSTANCE.WRITE_BACK_SCHEDULER.awaitTermination(30, TimeUnit.MINUTES);
        } catch (InterruptedException e) {
            throw new PersistenceException("Failed to save all data before returning from shutdown method.");
        }

        if (!terminated)
            throw new PersistenceException("Failed to save all data before returning from shutdown method.");

        INSTANCE.WRITE_BACK_WORKER.run();
    }
}

/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.api;

import pawnstudios.persistence.entities.EntityMap;
import pawnstudios.persistence.io.DataCreator;

public class Entity {
    public static <T extends PersistedData<T>> T getByIndex(String index, Object value, Class<T> entityClass) {
        return (T) Persistence.INSTANCE.INDEXED_RECORDS.getEntitySlow(index, value, entityClass);
    }

    public static <T extends PersistedData<T>> T getById(long id, Class<T> entityClass) {
        String indexName = getIdIndexName(entityClass);
        return getByIndex(indexName, id, entityClass);
    }

    public static <T extends PersistedData<T>> T getById(int id, Class<T> entityClass) {
        String indexName = getIdIndexName(entityClass);
        return getByIndex(indexName, id, entityClass);
    }

    public static <T extends PersistedData<T>> T getById(short id, Class<T> entityClass) {
        String indexName = getIdIndexName(entityClass);
        return getByIndex(indexName, id, entityClass);
    }

    public static <T extends PersistedData<T>> T getById(byte id, Class<T> entityClass) {
        String indexName = getIdIndexName(entityClass);
        return getByIndex(indexName, id, entityClass);
    }

    private static <T extends PersistedData<T>> String getIdIndexName(Class<T> entityClass) {
        EntityMap<T> entityMap = Persistence.INSTANCE.ENTITY_META_DATA.getEntityMap(entityClass);
        return entityMap.id.name;
    }

    public static <T extends PersistedData<T>> T create(Class<T> entityClass) {
        return DataCreator.createEntity(entityClass);
    }

}

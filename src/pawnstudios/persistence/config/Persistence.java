/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.config;

import pawnstudios.config.Config;
import pawnstudios.config.ConfigInitializer;

@Config
public class Persistence extends ConfigInitializer {
    public static String url;
    public static String user;
    public static String password;
    public static long saveIntervalMillis;
}

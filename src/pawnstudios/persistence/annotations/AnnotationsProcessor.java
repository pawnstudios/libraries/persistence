/*
 * Copyright (c) 2022  Alex (https://gitlab.com/CanIGetAnMR)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

package pawnstudios.persistence.annotations;

import org.reflections8.Reflections;
import org.reflections8.scanners.SubTypesScanner;
import org.reflections8.scanners.TypeAnnotationsScanner;
import org.reflections8.util.ClasspathHelper;
import org.reflections8.util.ConfigurationBuilder;
import pawnstudios.persistence.api.PersistedData;
import pawnstudios.persistence.entities.EntityMap;
import pawnstudios.persistence.entities.RecordMapping;
import pawnstudios.persistence.storage.EntityMetaData;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class AnnotationsProcessor {
    public static EntityMetaData processAnnotations() {
       return new EntityMetaData(getAnnotatedClasses()
          .stream()
          .collect(Collectors.toMap(entityClass -> entityClass, AnnotationsProcessor::processAnnotatedClass)));
    }

    private static <T extends PersistedData<?>> Set<Class<T>> getAnnotatedClasses() {
        Reflections reflections = new Reflections((new ConfigurationBuilder()
          .setScanners(new TypeAnnotationsScanner(), new SubTypesScanner())
          .setUrls(ClasspathHelper.forJavaClassPath())));
        Set<Class<?>> annotatedClasses = reflections.getTypesAnnotatedWith(Entity.class);
        
        return annotatedClasses
          .stream()
          .filter(PersistedData.class::isAssignableFrom)
          .map(entityClass -> (Class<T>) entityClass)
          .collect(Collectors.toUnmodifiableSet());
    }

    private static <T extends PersistedData<T>> EntityMap<T> processAnnotatedClass(Class<PersistedData<?>> annotatedClass) {
        Class<T> entity;
        String table;
        RecordMapping id;
        Map<String, RecordMapping> columns;
        Map<String, RecordMapping> indexes;
        
        entity = (Class<T>) annotatedClass;
        Table tableAnnotation = annotatedClass.getAnnotation(Table.class);
        table = tableAnnotation.name();
        id = Arrays.stream(annotatedClass.getDeclaredFields())
          .filter(field -> field.isAnnotationPresent(Id.class))
          .map(AnnotationsProcessor::getRecordMappingFromField)
          .findAny()
          .get();
        columns = Arrays.stream(annotatedClass.getDeclaredFields())
          .filter(field -> field.isAnnotationPresent(Column.class))
          .collect(getFieldsToRecordMappingsCollector());
        indexes = Arrays.stream(annotatedClass.getDeclaredFields())
          .filter(field -> field.isAnnotationPresent(Index.class))
          .collect(getFieldsToRecordMappingsCollector());

        return new EntityMap<>(entity, table, id, columns, indexes);
    }

    private static String getColumnNameFromField(Field field) {
        Column annotation = field.getAnnotation(Column.class);
        String columnName = field.getName();
        if (annotation != null && !annotation.name().equals("")) {
            columnName = annotation.name();
        }
        return columnName;
    }

    private static RecordMapping getRecordMappingFromField(Field field) {
        String columnName = getColumnNameFromField(field);
        return new RecordMapping(columnName, field);
    }

    private static Collector<Field, ?, Map<String, RecordMapping>> getFieldsToRecordMappingsCollector() {
        return Collectors.toMap(AnnotationsProcessor::getColumnNameFromField, AnnotationsProcessor::getRecordMappingFromField);
    }
}
